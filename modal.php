<!-- Modal Edit-->
<div class="modal fade" id="editModal<?php echo $res['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content shadow">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Редактировать запись № <?php echo $res['id'] ?></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
			<form action="?id=<?=$res['id'] ?>" method="post">
				<div class="form-group">
					<textarea class="form-control" name="edit_task_desc" value="<?php echo $res['task_desc'] ?>"><?php echo $res['task_desc'] ?></textarea>
				</div>
				<div class="form-group">
					<input type="text" class="form-control" name="edit_employee_name" value="<?php echo $res['employee_name'] ?>" placeholder="Ответстенный">
				</div>
				<div class="form-group">
					<input type="date" class="form-control" name="edit_deadline" value="<?php echo $res['deadline'] ?>">
				</div>
				<div class="form-group">
					<select class="form-control" name="edit_status">
						<option disabled>Статус задачи</option>
						<option value="in_progress">В работе</option>
						<option value="done">Завершена</option>
						<option value="awaiting">Ожидает выполнения</option>
						<option value="pause">На паузе</option>
					</select>
				</div>
				<div class="modal-footer">
					<button type="submit" name="edit-submit" class="btn btn-primary">Обновить</button>
				</div>
			</form>
		</div>
		</div>
	</div>
</div>

<!-- DELETE MODAL -->
<div class="modal fade" id="deleteModal<?php echo $res['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content shadow">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Удалить задачу № <?php echo $res['id'] ?></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
				<form action="?id=<?php echo $res['id'] ?>" method="post">
					<button type="submit" name="delete_submit" class="btn btn-danger">Удалить</button>
				</form>
			</div>
		</div>
	</div>
</div>

