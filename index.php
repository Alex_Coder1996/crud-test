<?php
require "db.php";
require "import.php";

?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Спискок дел</title>
</head>
<body>

<header>
    <h1 class="main_header">Список дел</h1>
</header>

<div class="container">

    <div class="table_container">
        <div class="tasks_list">
            <h3>Список дел</h3>
            <table>
                <?php foreach (array_reverse($result, true) as $res):?>
                    <tr>
                        <td><?php echo $res['id'] ?></td>
                        <td><?php echo $res['task_desc'] ?></td>
                        <td><?php echo $res['employee_name'] ?></td>
                        <td><?php echo $res['deadline'] ?></td>
                        <td><?php echo $res['status'] ?></td>
                        <td>
                            <a href="?edit=<?php echo $res['id'] ?>" class="btn btn-success btn-sm" data-toggle="modal" data-target="#editModal<?=$res['id'] ?>">Edit</a>
                            <a href="?delete=<?php echo $res['id'] ?>" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#deleteModal<?=$res['id'] ?>">Delete</a>
                        </td>
	                    <?php require 'modal.php'; ?>
                    </tr>
                <?php endforeach;?>
            </table>
        </div>

        <div class="add_new_task">
            <h3>Добавить новую задачу</h3>
            <form action="" method="post">
                <label for="">
                    <textarea placeholder="Описание задачи" name="task_desc"></textarea>
                </label><br/><hr/>
                <label for="">
                    <input type="text" placeholder="Ответственный" name="employee_name">
                </label><br/><hr/>
                <label for="">
                    <input type="date" placeholder="Срок" name="deadline">
                </label><br/><hr/>
                <label for="">
                    <select name="status">
                        <option disabled>Статус задачи</option>
                        <option value="in_progress">В работе</option>
                        <option value="done">Завершена</option>
                        <option selected value="awaiting">Ожидает выполнения</option>
                        <option value="pause">На паузе</option>
                    </select>
                </label><br/><hr/>
                <button type="submit" name="add_task">Добавить</button>
            </form>
        </div>
    </div>

    <div class="import-block">
        <h3>Импорт из таблицы(xlsx)</h3>
        <form id="form-filtr" action="" method="POST" enctype="multipart/form-data">
            Отправить этот файл:
            <input name="userfile" type="file" accept=".xlsx"/>
            <input type="submit" value="Отправить файл" />
        </form>
    </div>

</div>



<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>