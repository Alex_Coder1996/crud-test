<?php
require_once('vendor/autoload.php');
require "config.php";

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

if (isset($_FILES['userfile'])) {

	$uploaddir  = 'excel/';
	$uploadfile = $uploaddir . basename( $_FILES['userfile']['name'] );
	move_uploaded_file( $_FILES['userfile']['tmp_name'], $uploadfile );
	$file   = 'excel/' . $_FILES['userfile']['name'];
	$reader = IOFactory::createReaderForFile( $file );
	$reader->setReadDataOnly( true );

	// Читаем файл и записываем информацию в переменную
	$spreadsheet = $reader->load( $file );

	// Так можно достать объект Cells, имеющий доступ к содержимому ячеек
	$cells = $spreadsheet->getSheetByName( 'Лист1' )->getCellCollection();

	// Далее перебираем все заполненные строки
	for ( $row = 2; $row <= $cells->getHighestRow(); $row ++ ) {


		$array[ $row ]['task_desc']     = ( $cells->get( 'A' . $row ) ) ? $cells->get( 'A' . $row )->getValue() : '';
		$array[ $row ]['employee_name'] = ( $cells->get( 'B' . $row ) ) ? $cells->get( 'B' . $row )->getValue() : '';

		$date_tmp = ( ( ( $cells->get( 'C' . $row ) ) ? intval( $cells->get( 'C' . $row )->getValue() ) : '') - 25569) * 86400;
		$array[ $row ]['deadline']      = date( "Y-m-d", $date_tmp );

		$status_tmp = ( $cells->get( 'D' . $row ) ) ? $cells->get( 'D' . $row )->getValue() : '';
		if ( $status_tmp == 'Ожидает выполнения' ) {
			$status_tmp = 'awaiting';
		} elseif ($status_tmp == 'В работе') {
			$status_tmp = 'in progress';
		} elseif ($status_tmp == 'Пауза') {
			$status_tmp = 'pause';
		} elseif ( $status_tmp == 'Завершена' ) {
			$status_tmp = 'done';
		}
		$array[ $row ]['status']        = $status_tmp;
	}

	// connecting to db
	try {
		$pdo = new PDO( "mysql:host=$dbhost; dbname=$dbname", $dbuser, $dbpass );

		foreach($array as $value) {
			$sql   = ( "INSERT INTO `tasks`(`task_desc`, `employee_name`, `deadline`, `status`) VALUES(?,?,?,?)" );
			$query = $pdo->prepare( $sql );
			$query->execute( [ $value['task_desc'], $value['employee_name'], $value['deadline'], $value['status'] ] );
		}
		header('Location: '. $_SERVER['HTTP_REFERER']);
	} catch ( PDOException $e ) {
		echo "Connection failed: " . $e->getMessage();
	}

}