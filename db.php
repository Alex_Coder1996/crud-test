<?php

require "config.php";

// connecting to db
try {
	$pdo = new PDO( "mysql:host=$dbhost; dbname=$dbname", $dbuser, $dbpass );
} catch ( PDOException $e ) {
	echo "Connection failed: " . $e->getMessage();
}

// Create
$task_desc = $_POST['task_desc'];
$employee_name = $_POST['employee_name'];
$deadline = $_POST['deadline'];
$status = $_POST['status'];

if ( isset($_POST['add_task']) && $_POST['employee_name'] && $_POST['deadline'] ) {
	$sql   = ( "INSERT INTO `tasks`(`task_desc`, `employee_name`, `deadline`, `status`) VALUES(?,?,?,?)" );
	$query = $pdo->prepare( $sql );
	$query->execute( [ $task_desc, $employee_name, $deadline, $status ] );
}

// Read
$sql = $pdo->prepare("SELECT * FROM `tasks`");
$sql->execute();
$result = $sql->fetchAll();

// Update
$edit_task_desc = $_POST['edit_task_desc'];
$edit_employee_name = $_POST['edit_employee_name'];
$edit_deadline = $_POST['edit_deadline'];
$edit_status = $_POST['edit_status'];
$get_id = $_GET['id'];
if (isset($_POST['edit-submit'])) {
	$sqll = "UPDATE tasks SET task_desc=?, employee_name=?, deadline=?, status=? WHERE id=?";
	$querys = $pdo->prepare($sqll);
	$querys->execute([ $edit_task_desc, $edit_employee_name, $edit_deadline, $edit_status, $get_id ]);
	header('Location: '. $_SERVER['HTTP_REFERER']);
}

// Delete
if (isset($_POST['delete_submit'])) {
	$sqld = "DELETE FROM `tasks` WHERE `tasks`.`id`=?";
	$queryd = $pdo->prepare($sqld);
	$queryd->execute([$get_id]);
	header('Location: '. $_SERVER['HTTP_REFERER']);
}